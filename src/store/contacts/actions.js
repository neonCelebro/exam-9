import axios from '../../axios';

export const FETCH_MENU_SUCCESS = "FETCH_MENU_SUCCESS";
export const FETCH_REQUEST = "FETCH_REQUEST";
export const FETCH_ERROR = "FETCH_ERROR";
export const EDIT_CONTACT = 'EDIT_CONTACT';
export const STOP_EDIT_CONTACT = 'STOP_EDIT_CONTACT';

export const fetchRequest = () => {
    return {type: FETCH_REQUEST}
};

export const fetchError = () => {
    return {type: FETCH_ERROR}
};

export const fetchMenuSuccess = (data) => {
    return {type: FETCH_MENU_SUCCESS, data};
};

export const fetchEditContact = (data) => {
    return {type: EDIT_CONTACT, data};
};

export const stopEditContact = () => {
    return {type: STOP_EDIT_CONTACT}
};

export const addNameToBase = (data) => {
    return (dispatch) => {
        dispatch(fetchRequest());
        axios.post('./contacts.json', data)
    }
};

export const editNameToBase = (data, id) => {
    return (dispatch) => {
        dispatch(fetchRequest());
        axios.put(`./contacts/${id}.json`, data).then(() => {
            axios.get('./contacts.json')
                .then(response => {
                    console.log(Object.val);
                    dispatch(fetchMenuSuccess(response.data));
                }, error => {
                    dispatch(fetchError(error))
                })
        })
    }
};

export const fetchContacts = () => {
    return (dispatch) => {
        dispatch(fetchRequest());
        axios.get('./contacts.json')
            .then(response => {
                console.log(Object.val);
                dispatch(fetchMenuSuccess(response.data));
            }, error => {
                dispatch(fetchError(error))
            })
    }
};

export const editContacts = (id) => {
    return (dispatch) => {
        dispatch(fetchRequest());
        axios.get(`./contacts/${id}.json`)
            .then(response => {
                dispatch(fetchEditContact({[id]: response.data}));
            }, error => {
                dispatch(fetchError(error))
            })
    }
};

export const deleteContactsFromBase = (id) => {
    return (dispatch) => {
        dispatch(fetchRequest());
        axios.delete(`./contacts/${id}.json`).then(()=> {
            axios.get('./contacts.json')
                .then(response => {
                    console.log(Object.val);
                    dispatch(fetchMenuSuccess(response.data));
                }, error => {
                    dispatch(fetchError(error))
                })
        });
    }
};