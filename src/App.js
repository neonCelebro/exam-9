import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import Layout from './components/UI/Layout/Layout';
import Contacts from './containers/Contacts/Contacts';
import AddNewContact from './containers/AddNewContact/AddNewContact';
import ModalEdit from "./containers/ModalEdit/ModalEdit";
import Editor from "./containers/Editor/Editor";
import './App.css';



class App extends Component {
  render() {
    return (
        <Layout>
          <Switch>
            <Route path="/" exact component={Contacts}/>
            <Route path="/addNewContact" component={AddNewContact}/>
              <Route path="/editContact" component={ModalEdit}/>
              <Route path="/editor" component={Editor}/>
          </Switch>
        </Layout>
    );
  }
}

export default App;
