import React from 'react';
import {NavLink} from 'react-router-dom';
import './Header.css';

const Header = () => {
    return (
        <header className="Toolbar">
            <nav>
                <NavLink className="Link" to="/">Contacts</NavLink>
                <NavLink className="Link" to="/addNewContact">Add New Contact</NavLink>
            </nav>
        </header>
    )
};

export default Header;