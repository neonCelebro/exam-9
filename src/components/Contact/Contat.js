import React from 'react';
import { connect } from 'react-redux';

import './Contact.css';
import {editContacts, stopEditContact} from "../../store/contacts/actions";

const Contact = props => {
    const contacts = Object.values(props.contact);
    const IDs = Object.keys(props.contact);
    return (
        contacts.map((contact, index) => {
            return (
                <div onClick={()=> props.editContacts(IDs[index])} className="contact" key={index}>
                    <img src={contact.photo} alt="img"/>
                    <h4>{contact.name}</h4>
                </div>
            )
        })
    )
};

const mapDispatchToProps = dispatch => {
    return {
        editContacts: (id) => dispatch(editContacts(id)),
    }
}

export default connect(null, mapDispatchToProps)(Contact);