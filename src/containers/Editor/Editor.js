import React, { Component, Fragment } from 'react';
import './Editor.css';
import { connect } from 'react-redux';
import {editNameToBase} from "../../store/contacts/actions";

class EditContact extends Component {
    state = {
        name: Object.values(this.props.edited)[0].name,
        number: Object.values(this.props.edited)[0].number,
        email: Object.values(this.props.edited)[0].email,
        photo: Object.values(this.props.edited)[0].photo,
    };

    getName = (event) => {
        this.setState({name: event.target.value})
    };

    getNumber = (event) => {
        this.setState({number: event.target.value});
    };

    getPhoto = (event) => {
        this.setState({photo: event.target.value})
    };
    getEmail = (event) => {
        this.setState({email: event.target.value})
    };

    sendNameHandler = event => {
        console.log(Object.keys(this.props.edited));
        event.preventDefault();
        this.props.editToBase(this.state, Object.keys(this.props.edited));
        this.props.history.push('/');
    };

        render() {
            console.log(this.props.edited);
            return (
                <Fragment>
                    <form className="form" onSubmit={this.sendNameHandler}>

                        <div className='photoPrev'>
                            <img src={this.state.photo} alt='#'/>
                        </div>

                        <h1>Edit contact</h1>
                        <input
                            required
                            value={this.state.name}
                            onChange={this.getName}
                            type="text"
                            placeholder="enter name"
                        />

                        <input
                            required
                            value={this.state.number}
                            onChange={this.getNumber}
                            type="number"
                            placeholder="enter number telephone"
                        />

                        <input
                            value={this.state.email}
                            onChange={this.getEmail}
                            type="email"
                            placeholder="enter email"
                        />

                        <input
                            value={this.state.image}
                            onChange={this.getPhoto}
                            type="text"
                            placeholder="enter URL of image"
                        />

                        <input type='submit' value='Edit Contact'/>

                    </form>
                </Fragment>
            )
        }
    }

const mapStateToProps = state => {
    return {
        edited: state.edited,
    }
};
const mapDispatchToProps = dispatch => {
    return {
        editToBase: (data, id) => dispatch(editNameToBase(data, id)),
}
};

export default connect(mapStateToProps, mapDispatchToProps)(EditContact);