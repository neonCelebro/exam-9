import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import {fetchContacts} from "../../store/contacts/actions";
import { NavLink } from 'react-router-dom';

import Contact from '../../components/Contact/Contat';
import './Contacts.css';
import EditContact from "../ModalEdit/ModalEdit";

class Contacts extends Component {

    componentDidMount() {
        this.props.fetchContacts();
    }

    render() {
        return (
            <Fragment>
                {this.props.editStatus? <EditContact {...this.props} /> : null}
                <div className="container">
                    <NavLink className='addNewContact' to="/addNewContact">Add New Contact</NavLink>
                    <h3>List of contacts</h3>
                    <Contact
                        contact={this.props.contacts}
                    />
                </div>
            </Fragment>

        )
    }
}

const mapStateToProps = state => {
    return {
        contacts: state.contacts,
        editStatus: state.editStatus,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        fetchContacts: () => dispatch(fetchContacts()),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Contacts);