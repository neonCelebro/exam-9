import React, { Component} from 'react';
import { connect } from 'react-redux';
import {deleteContactsFromBase, stopEditContact} from "../../store/contacts/actions";

import './ModalEdit.css';

class EditContact extends Component {

    deleteContactHendler = (id) => {
        this.props.deleteDishFromBase(id);
    };

    render() {
        const contacts = Object.values(this.props.contacts);
        console.log(Object.keys(this.props.contacts));
        return (
            <div onClick={this.props.stopEdit} className='wrapper'>
                <div className="modal">
                    <img src={contacts[0].photo} alt="img"/>
                    <h4>{contacts[0].name}</h4>
                    <p>Number: {contacts[0].number}</p>
                    <p>Email: {contacts[0].email}</p>
                    <button onClick={() => this.props.history.push('editor')}>Edit</button>
                    <button onClick={() => this.deleteContactHendler(Object.keys(this.props.contacts))}>Delete</button>
                </div>
            </div>

        )
    }
}

const mapStateToProps = state => {
    return {
        contacts: state.edited,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        deleteDishFromBase: (id) => dispatch(deleteContactsFromBase(id)),
        stopEdit: (id) => dispatch(stopEditContact(id)),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(EditContact);