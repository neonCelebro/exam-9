import React, { Component, Fragment } from 'react';
import './AddNewContat.css';
import { connect } from 'react-redux';
import {addNameToBase} from "../../store/contacts/actions";

class AddNewContact extends Component {

    state = {
        name: '',
        number: '',
        email: '',
        photo: 'http://s1.iconbird.com/ico/2014/1/567/w512h5121389807811officeaddressbookicon.png',
    };

    getName = (event) => {
        this.setState({name: event.target.value})
    };

    getNumber = (event) => {
        this.setState({number: event.target.value});
    };

    getPhoto = (event) => {
        this.setState({photo: event.target.value})
    };
    getEmail = (event) => {
        this.setState({email: event.target.value})
    };

    sendNameHandler = event => {
        event.preventDefault();
        this.props.addNameToBase(this.state);
        this.setState({
            name: '',
            number: '',
            email: '',
            photo: 'http://s1.iconbird.com/ico/2014/1/567/w512h5121389807811officeaddressbookicon.png'});
    };

    render() {
        return (
            <Fragment>
                <form className="form" onSubmit={this.sendNameHandler}>

                        <div className='photoPrev'>
                            <img src={this.state.photo} alt='#'/>
                        </div>

                    <h1>Add new contact</h1>
                    <input
                        required
                        value={this.state.name}
                        onChange={this.getName}
                        type="text"
                        placeholder="enter name"
                    />

                    <input
                        required
                        value={this.state.number}
                        onChange={this.getNumber}
                        type="number"
                        placeholder="enter number telephone"
                    />

                    <input
                        value={this.state.email}
                        onChange={this.getEmail}
                        type="email"
                        placeholder="enter email"
                    />

                    <input
                        value={this.state.image}
                        onChange={this.getPhoto}
                        type="text"
                        placeholder="enter URL of image"
                    />

                    <input type='submit' value='Add New Contact'/>

                 </form>
            </Fragment>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        addNameToBase: (data) => dispatch(addNameToBase(data))
    }
};

export default connect(null, mapDispatchToProps)(AddNewContact);